module.exports = {
  pwa: {
    display: 'standalone',
    background_color: '#000000',
    themeColor: '#000000',
    msTileColor: '#000000',
    backgroundColor: '#000000',
    theme_color: '#000000',
    manifestOptions: {
      name: 'Mon travail 4',
      short_name: 'Travail 4'
    }
  }
}
