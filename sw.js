var nomCache = 'ma-pwa'
var fichiersMettreCache = [
  '/index.html',
  '/main.js',
  '/manifest.json',
  '/favicon.ico',
  'https://code.jquery.com/jquery-3.6.0.min.js'
]

// https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/install_event
self.addEventListener('install', function (e) {
  e.waitUntil(
    caches.open(nomCache).then(function (cache) {
      return cache.addAll(fichiersMettreCache)
    })
  )
})

// https://developer.mozilla.org/fr/docs/Web/API/FetchEvent
self.addEventListener('fetch', function (event) {
  if (!(event.request.url.indexOf('http') === 0)) return // on ne veut pas autre chose que du http
  event.respondWith(
    caches.match(event.request) // le fichier est dans la cache
      .then(function (response) {
        var fetchPromise = fetch(event.request).then(
          function (networkResponse) {
            caches.open(nomCache).then(function (cache) {
              cache.put(event.request, networkResponse.clone())
              return networkResponse
            })
          })
          .catch(function () {
            console.log('Hors-ligne?')
          })
        return response || fetchPromise
      })
  )
})
