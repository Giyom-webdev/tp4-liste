| Cégep FX-Garneau |
| --- |
|
# Documentation du projet : TP4-Liste d&#39;épicerie
 |
| Session Hiver 2021 |

| Guillaume Gosselin-Bédard et Benoit Cormier-Gosselin

 |
| --- |

# Table des matières

[Technologies utilisées 1](#_Toc73299894)

[**Fonction** 1](#_Toc73299895)

[**Technologies** 1](#_Toc73299896)

[Maquettes 2](#_Toc73299897)

[Bogues et améliorations futures 3](#_Toc73299898)

[Gestion de projet 4](#_Toc73299899)

[Figure 1 - Kitchen Sink 2](#_Toc73283610)

[Figure 2 - Maquette mobile 3](#_Toc73283611)

[Figure 3 - Maquette Desktop 3](#_Toc73283612)

##

## Technologies utilisées

###

|
### **Fonction**
 |
### **Technologies**
 |
| --- | --- |
| Fonctionnement de base de l&#39;application | Serveur PythonWAMP |
| Rendu visuel de l&#39;application | Vue.jsBoostrapFavicon |
| Validation de champ | JQuery |
| Organisation du code | SMACSSSASS |
| Autocomplétion | AJAX |
| Tests de validité | Selenium |

###

# Maquettes

Nous avons essayé de respecter le plus possible les maquettes. Toutefois, la page d&#39;accueil ne présente pas les listes enregistrées, mais plutôt un lien vers chacune d&#39;entre elles. La fenêtre qui présente les produits disponibles ne présente pas d&#39;image pour accompagner chaque produit.

![](RackMultipart20210531-4-21xune_html_717bf2fabd6512af.png)

_Figure 1 - Kitchen Sink_

![](RackMultipart20210531-4-21xune_html_cf6a6911f3917c37.png)

_Figure 2 - Maquette mobile_

![](RackMultipart20210531-4-21xune_html_a5a85fd29bb41502.png)

_Figure 3 - Maquette Desktop_

# Gestion de projet

Au fur et à mesure que nous avons progressé dans le projet, nous avons réalisé que nous avions mal compris certains aspects du mandat, dont la façon que fonctionne l&#39;application. Par exemple, nous pensions que les 5 pages seraient en HTML. Le temps de comprendre comment maîtriser certaines technologies et déboguer l&#39;installation de celles-ci, nous n&#39;avons pas respecté les dates fixées au début du mandat. Il n&#39;est pas évident d&#39;estimer le temps de production lorsque c&#39;est la première fois que nous réalisons une application de la sorte. Malgré tout, nous avons réussi à compléter toutes les tâches demandées pour la date de remise.

![](RackMultipart20210531-4-21xune_html_5b076a5039edb20f.png)

_Figure 4 - Tableau Trello_

# Bogues et améliorations futures

1. Aspect responsive : Certains éléments des fenêtres ne sont pas _responsive_.
2. Accessibilité : Des détails pourraient être ajoutés pour rendre accessible l&#39;application aux lecteurs d&#39;écran.
3. Recherche : L&#39;autocomplétion du champ _Recherche_ par mot clé au lieu de ID (numéros).